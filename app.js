const express = require('express')

const app = express();
const db = require('./node-mysql-db')
// const bodyParser = require('body-parser')



db.connect( (err) => {
    if(err) {
        throw err;
    }
    console.log('MySQL connected...');
})

// app.use(bodyParser.urlencoded({ extended: true }))
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.send('home page')
})

app.get('/getResource', (req, res) => {
    let sql = "SELECT *, DATE_FORMAT(date, '%d %b %Y %H:%i') as formattedDate FROM journal "
    // let sql = "SELECT *, DATE_FORMAT(date, '%d %b %Y %H:%i') as formattedDate FROM journal WHERE id=3 "
    let query = db.query(sql, (err, results) => {
        if (err) throw err
        // console.log(results)
        // res.json(results)
        res.render('home', {results:results})
    })
})

// create DB
app.get('/createdb', (req, res) => {
    let sql = 'CREATE DATABASE nodemysql';
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('database created')
    })
})

app.get('/createPostsTable', (req, res) => {
    let sql = "CREATE TABLE posts (id int AUTO_INCREMENT, title VARCHAR(255), body VARCHAR(255), PRIMARY KEY (id) )" ;
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Posts table created');
        
    })
})

app.get('/addPost1', (req, res) => {
    let post = {title: 'Post One', body: 'This is post number one'}
    let sql = 'INSERT INTO posts SET ?'
    let query = db.query(sql, post, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.send('Post 1 added and the id is : ' + results.insertId);
        
    })
})

app.get('/getPosts', (req, res) => {
    let sql = 'SELECT * FROM posts'
    let query = db.query(sql, (err, results) => {
        if(err) throw err
        console.log(results)
        res.send('Posts fetched')
    })
})


app.get('/getPost/:title', (req, res) => {
    let sql = `SELECT * FROM posts WHERE title = ${req.params.title}`
    let query = db.query(sql, (err, result) => {
        if(err) throw err
        console.log(result)
        // res.sendn('Post fetched...)
        res.json(result)
    })
})

app.get('/updatePost/:id/:title', (req, res) => {
    let newTitle = 'Updated title'
    let sql = `UPDATE posts SET title = '${req.params.title}' WHERE id = ${req.params.id}`
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Post updated...')
    })
})

app.get('/deletePost/:id', (req, res) => {
    let sql = `DELETE FROM posts WHERE id = ${req.params.id}`
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Post deleted...')
        
    })
})

app.listen(port=3000, () => {
    console.log('Server started on port', port);
    
})