const mysql = require('mysql')
const dbInfo = require('../db-mysql')

const db = mysql.createConnection({
    // host    : 'localhost',
    // host    : '11.1.1.58',
    // user    :  'root',
    // password: '',
    // database: 'nazs'
    // host    : '77.104.180.152',
    host: dbInfo.host,
    user: dbInfo.user,
    password: dbInfo.pwd,
    database: dbInfo.database
})
 
module.exports = mysql
module.exports = db